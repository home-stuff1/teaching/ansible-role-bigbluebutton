---
- name: register current bbb properties
  slurp:
    src: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
  register: properties

- name: set bbb hostname
  command: "bbb-conf --setip {{ bbb_hostname }}"
  become: true
  when: (properties['content'] | b64decode | regex_search(bbb_hostname, multiline=True, ignorecase=True) != bbb_hostname) or
    _bbb_install.changed or
    _bbb_upgrade.changed
  notify: restart bigbluebutton

- name: enable join via html5
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^attendeesJoinViaHTML5Client'
    line: 'attendeesJoinViaHTML5Client=true'
  notify: restart bigbluebutton

- name: enable mod join via html5
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^moderatorsJoinViaHTML5Client'
    line: 'moderatorsJoinViaHTML5Client=true'
  notify: restart bigbluebutton

- name: set bbb to use HTTPS
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^bigbluebutton.web.serverURL'
    line: 'bigbluebutton.web.serverURL=https://{{ bbb_hostname }}'
  notify: restart bigbluebutton

- name: set recording default
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^disableRecordingDefault'
    line: 'disableRecordingDefault={{ bbb_disable_recordings | ternary("true", "false") }}'
  notify: restart bigbluebutton

- name: ensure recording auto start is disabled
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^autoStartRecording'
    line: 'autoStartRecording=false'
  notify: restart bigbluebutton

- name: allow/disable users to start recordings
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^allowStartStopRecording'
    line: 'allowStartStopRecording={{ bbb_disable_recordings | ternary("false", "true") }}'
  notify: restart bigbluebutton

- name: set muteOnStart
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^muteOnStart'
    line: 'muteOnStart={{ bbb_mute_on_start | ternary("true", "false") }}'
  notify: restart bigbluebutton

- name: set appLogLevel
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^appLogLevel'
    line: 'appLogLevel={{ bbb_app_log_level }}'
  notify: restart bigbluebutton

- name: register bbb secret
  command: bbb-conf --secret
  changed_when: false
  register: result

- name: parse bbb secret
  set_fact:
    current_bbb_secret: "{{ result.stdout | regex_search('Secret: ([a-zA-Z0-9]*)', multiline=True) |  regex_replace('Secret: ') }}"
    cacheable: true

- name: set BBB secret
  command: "bbb-conf --setsecret {{ bbb_secret }}"
  when: bbb_secret is defined and bbb_secret != current_bbb_secret
  notify: restart bigbluebutton

- name: set bbb secret variable
  set_fact:
    bbb_secret: "{{ current_bbb_secret }}"
    cacheable: true
  when: not bbb_secret is defined

- name: set bbb Guest Policy
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultGuestPolicy='
    line: 'defaultGuestPolicy={{ bbb_guestpolicy }}'
  notify: restart bigbluebutton

- name: set red5 to use HTTPS 1/2
  lineinfile:
    path: /usr/share/red5/webapps/screenshare/WEB-INF/screenshare.properties
    regexp: '^jnlpUrl'
    line: 'jnlpUrl=https://{{ bbb_hostname }}/screenshare'
  notify: restart bigbluebutton

- name: set red5 to use HTTPS 2/2
  lineinfile:
    path: /usr/share/red5/webapps/screenshare/WEB-INF/screenshare.properties
    regexp: '^jnlpFile'
    line: 'jnlpFile=https://{{ bbb_hostname }}/screenshare/screenshare.jnlp'
  notify: restart bigbluebutton

- name: set bbb default welcome message
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultWelcomeMessage='
    line: 'defaultWelcomeMessage={{ bbb_default_welcome_message }}'
  notify: restart bigbluebutton

- name: set bbb default welcome message footer
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultWelcomeMessageFooter='
    line: 'defaultWelcomeMessageFooter={{ bbb_default_welcome_message_footer }}'
  when: not bbb_dialin_enabled or not bbb_dialin_overwrite_footer
  notify: restart bigbluebutton

- name: set bbb default welcome message footer for dial-in
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultWelcomeMessageFooter='
    line: 'defaultWelcomeMessageFooter={{ bbb_dialin_footer }}'
  when: bbb_dialin_enabled and bbb_dialin_overwrite_footer
  notify: restart bigbluebutton

- name: set bbb default presentation
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^beans.presentationService.defaultUploadedPresentation='
    line: 'beans.presentationService.defaultUploadedPresentation={{ bbb_default_presentation }}'
  notify: restart bigbluebutton

- name: copy TURN config file
  template:
    src: bbb/turn-stun-servers.xml.j2
    dest: /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
  notify: restart bigbluebutton

- name: serve recordings via https
  lineinfile:
    path: /usr/local/bigbluebutton/core/scripts/bigbluebutton.yml
    regexp: "playback_protocol"
    line: "playback_protocol: https"
  notify: restart bigbluebutton

- name: set BigBlueButton client to load components via HTTPS
  notify: restart bigbluebutton
  replace:
    path: /var/www/bigbluebutton/client/conf/config.xml
    regexp: 'http://'
    replace: 'https://'

- name: set note server to use HTTPS
  notify: restart bigbluebutton
  replace:
    path: /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
    regexp: 'http://'
    replace: 'https://'

- name: set kurento server to use HTTPS
  notify: restart bigbluebutton
  replace:
    path: /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
    regexp: 'ws://'
    replace: 'wss://'

- name: Read meteor
  slurp:
    path: /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
  register: r_meteor

- name: extract config of meteor
  set_fact:
    meteor: "{{ r_meteor['content'] | b64decode | from_yaml }}"

- name: combine meteor config
  set_fact:
    meteor: "{{ meteor | combine(bbb_meteor, recursive=true) }}"

- name: write back new meteor config
  notify: restart bigbluebutton
  copy:
    content: '{{ meteor  | to_nice_yaml }}'
    dest: /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml

- name: configure freeswitch to use SSL
  lineinfile:
    path: /etc/bigbluebutton/nginx/sip.nginx
    regexp: "proxy_pass"
    line: "        proxy_pass https://{{ bbb_freeswitch_ip_address }}:7443;"
  notify: reload nginx

- name: create ssl folder for nginx
  file:
    path: /etc/nginx/ssl
    state: directory

- name: Copy Predefined DH group ffdhe2048
  copy:
    src: ffdhe4096.pem
    dest: /etc/nginx/ssl/ffdhe4096.pem
    owner: root
    group: root
    mode: '0644'
  notify: reload nginx

- name: copy bbb's nginx vhost
  template:
     src: bbb/bigbluebutton.j2
     dest: /etc/nginx/sites-available/bigbluebutton
  notify: reload nginx

- name: configure CPU and IO Scheduling in FreeSWITCH service
  lineinfile:
    path: /lib/systemd/system/freeswitch.service
    regexp: '{{ item.regexp }}'
    line: '{{ item.line }}'
    state: '{{ item.state }}'
    insertbefore: '^CPUSchedulingPriority='
  with_items:
    - line: 'CPUSchedulingPolicy=rr'
      regexp: '^#?CPUSchedulingPolicy='
      state: '{{ "present" if bbb_cpuschedule | bool else "absent" }}'
    - line: 'IOSchedulingClass=realtime'
      regexp: '^#?IOSchedulingClass='
      state: '{{ "present" if bbb_freeswitch_ioschedule_realtime | bool else "absent" }}'
  notify:
  - restart freeswitch
  - reload systemd

- name: configure IP {{ bbb_freeswitch_ip_address }} for wss-binding of FreeSWITCH
  lineinfile:
    path: /opt/freeswitch/conf/sip_profiles/external.xml
    regexp: "<param name=\"wss-binding\"  value=\""
    line: "    <param name=\"wss-binding\"  value=\"{{ bbb_freeswitch_ip_address }}:7443\"/>"
  notify: restart freeswitch

- name: enable IPv6 in FreeSWITCH service 1/2
  command:
    cmd: mv {{ item }}_ {{ item }}
    creates: "{{ item }}"
    removes: "{{ item }}_"
  with_items:
    - "/opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml"
    - "/opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml"
  when: bbb_freeswitch_ipv6 | bool
  notify:
  - restart freeswitch
  - reload nginx

- name: enable IPv6 in FreeSWITCH service 2/2
  lineinfile:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml
    regexp: '<param name="listen-ip" value="[a-fA-F0-9.:]+"/>'
    line: '<param name="listen-ip" value="::"/>'
  when: bbb_freeswitch_ipv6 | bool
  notify:
  - restart freeswitch
  - reload nginx

- name: disable IPv6 in FreeSWITCH service 1/2
  command:
    cmd: mv {{ item }} {{ item }}_
    creates: "{{ item }}_"
    removes: "{{ item }}"
  with_items:
    - "/opt/freeswitch/etc/freeswitch/sip_profiles/internal-ipv6.xml"
    - "/opt/freeswitch/etc/freeswitch/sip_profiles/external-ipv6.xml"
  when: not bbb_freeswitch_ipv6 | bool
  notify:
  - restart freeswitch
  - reload nginx

- name: disable IPv6 in FreeSWITCH service 2/2
  lineinfile:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml
    regexp: '<param name="listen-ip" value="[a-fA-F0-9.:]+"/>'
    line: '<param name="listen-ip" value="127.0.0.1"/>'
  when: not bbb_freeswitch_ipv6 | bool
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH dialplan quality
  replace:
    path: /opt/freeswitch/conf/dialplan/default/bbb_conference.xml
    regexp: '<action application="conference" data="\$1@.*"\/>'
    replace: '<action application="conference" data="$1@{{ bbb_dialplan_quality }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH dialplan quality (echo)
  replace:
    path: /opt/freeswitch/conf/dialplan/default/bbb_echo_to_conference.xml
    regexp: '<action application="conference" data="\${vbridge}@.*"\/>'
    replace: '<action application="conference" data="${vbridge}@{{ bbb_dialplan_quality }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH dialplan energy-level
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    after: '<profile name="{{ bbb_dialplan_quality }}">'
    before: '</profile>'
    regexp: '<param name="energy-level" value=".*"\/>'
    replace: '<param name="energy-level" value="{{ bbb_dialplan_energy_level }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH dialplan comfort-noise
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    after:   '<profile name="{{ bbb_dialplan_quality }}">'
    before:  '</profile>'
    regexp:  '<param name="comfort-noise" value=".*"\/>'
    replace: '<param name="comfort-noise" value="{{ bbb_dialplan_comfort_noise }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH external ip (rtp)
  replace:
    path: /opt/freeswitch/etc/freeswitch/vars.xml
    regexp:  '<X-PRE-PROCESS cmd="set" data="external_rtp_ip=.*"\/>'
    replace: '<X-PRE-PROCESS cmd="set" data="external_rtp_ip={{ bbb_freeswitch_external_ip }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: FreeSWITCH external ip (sip)
  replace:
    path: /opt/freeswitch/etc/freeswitch/vars.xml
    regexp:  '<X-PRE-PROCESS cmd="set" data="external_sip_ip=.*"\/>'
    replace: '<X-PRE-PROCESS cmd="set" data="external_sip_ip={{ bbb_freeswitch_external_ip }}"/>'
  notify:
  - restart freeswitch
  - reload nginx

- name: set nodejs options for the html5-webclient
  replace:
    path: /usr/share/meteor/bundle/systemd_start.sh
    regexp:  'PORT=3000 /usr/share/\$NODE_VERSION/bin/node.*main.js'
    replace: 'PORT=3000 /usr/share/$NODE_VERSION/bin/node {{ bbb_html5_node_options }} main.js'
  when: bbb_html5_node_options is defined
  notify: restart bigbluebutton

- name: unset nodejs options for the html5-webclient
  replace:
    path: /usr/share/meteor/bundle/systemd_start.sh
    regexp:  'PORT=3000 /usr/share/\$NODE_VERSION/bin/node.*main.js'
    replace: 'PORT=3000 /usr/share/$NODE_VERSION/bin/node main.js'
  when: not bbb_html5_node_options is defined
  notify: restart bigbluebutton

- name: Create overrides directory for bbb-html5.service
  file:
    path: /etc/systemd/system/bbb-html5.service.d
    state: directory
  when: bbb_cpuschedule == false

- name: Save override file for bbb-html5.service
  copy:
    dest: /etc/systemd/system/bbb-html5.service.d/override.conf
    content: |
      [Service]
      # Set no scheduling policy in LXC, see https://docs.bigbluebutton.org/2.2/troubleshooting.html#bbb-html5-fails-to-start-with-a-setscheduler-error
      CPUSchedulingPolicy=other
      Nice=-10
  when: bbb_cpuschedule == false
  notify:
    - reload systemd
    - restart bigbluebutton

- name: Remove override file for bbb-html5.service
  file:
    path: /etc/systemd/system/bbb-html5.service.d/override.conf
    state: absent
  when: bbb_cpuschedule == true
  notify:
    - reload systemd
    - restart bigbluebutton

- name: Set the default maximum number of users a meeting can have
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultMaxUsers='
    line: 'defaultMaxUsers={{ bbb_default_max_users }}'
  notify: restart bigbluebutton

- name: Set default duration of the meeting (in minutes)
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^defaultMeetingDuration='
    line: 'defaultMeetingDuration={{ bbb_default_meeting_duration }}'
  notify: restart bigbluebutton

- name: Set the maximum number of pages allowed for an uploaded presentation
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^maxNumPages='
    line: 'maxNumPages={{ bbb_max_num_pages }}'
  notify: restart bigbluebutton

- name: Set the max allowed number of minutes the conversion should take
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^maxConversionTime='
    line: 'maxConversionTime={{ bbb_max_conversion_time }}'
  notify: restart bigbluebutton

- name: Set the number of threads in the pool to do the presentation conversion
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^numConversionThreads='
    line: 'numConversionThreads={{ bbb_num_conversion_threads }}'
  notify: restart bigbluebutton

- name: Set the number of threads to process file uploads
  lineinfile:
    path: /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    regexp: '^numFileProcessorThreads='
    line: 'numFileProcessorThreads={{ bbb_num_file_processor_threads }}'
  notify: restart bigbluebutton

- name: Set the retention period for presentations, red5, kurento, and freeswitch caches
  lineinfile:
    path: /etc/cron.daily/bigbluebutton
    regexp: '^history'
    line: 'history={{ bbb_cron_history }}'

- name: Set the retention period of recordings for meetings with no recording markers
  lineinfile:
    path: /etc/cron.daily/bigbluebutton
    regexp: '^unrecorded_days'
    line: 'unrecorded_days={{ bbb_cron_unrecorded_days }}'

- name: Set the retention period of recordings’ raw data
  lineinfile:
    path: /etc/cron.daily/bigbluebutton
    regexp: '^published_days'
    line: 'published_days={{ bbb_cron_published_days }}'

- name: Set the retention period of old log files
  lineinfile:
    path: /etc/cron.daily/bigbluebutton
    regexp: '^log_history'
    line: 'log_history={{ bbb_cron_log_history }}'

- name: Disable muted sound
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    regexp: '<param name="muted-sound" value="conference/conf-muted.wav"/>'
    replace: '<!-- <param name="muted-sound" value="conference/conf-muted.wav"/> -->'
  when: bbb_freeswitch_muted_sound == false
  notify: restart bigbluebutton

- name: Disable unmuted sound
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    regexp: '<param name="unmuted-sound" value="conference/conf-unmuted.wav"/>'
    replace: '<!-- <param name="unmuted-sound" value="conference/conf-unmuted.wav"/> -->'
  when: bbb_freeswitch_unmuted_sound == false
  notify: restart bigbluebutton

- name: Enable muted sound
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    regexp: '<!-- <param name="muted-sound" value="conference/conf-muted.wav"/> -->'
    replace: '<param name="muted-sound" value="conference/conf-muted.wav"/>'
  when: bbb_freeswitch_muted_sound == true
  notify: restart bigbluebutton

- name: Enable unmuted sound
  replace:
    path: /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml
    regexp: '<!-- <param name="unmuted-sound" value="conference/conf-unmuted.wav"/> -->'
    replace: '<param name="unmuted-sound" value="conference/conf-unmuted.wav"/>'
  when: bbb_freeswitch_muted_sound == true
  notify: restart bigbluebutton
